from django.shortcuts import render
from todos.models import TodoList


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_list": todo_lists,
    }
    return render(request, "todo_lists/list.html", context)